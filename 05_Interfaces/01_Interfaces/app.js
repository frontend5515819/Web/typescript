// interaces define contracts 
// interfaces in javascript not only describe contracts for classes, but also for objects and functiosn 
// the interface can be treated as a type
// this fucntion require an aircraft to be passed 
function checkAircraft(aircraft) {
    return aircraft.identification.substring(0, aircraft.manufacturer.length) == aircraft.manufacturer;
}
// the object impelemnts the interfaces, if it contains all the properties above
// so extra properties still makes the object an implementation of the interface
var customAircraft = {
    identification: "Airbus A380",
    manufacturer: "Airbus",
    engines: "CFM 56-5",
    manufacturingDate: new Date(Date.now()),
    lastMaintenanceCheck: new Date("2023-09-03")
};
console.log(checkAircraft(customAircraft));
console.log(customAircraft.manufacturingDate);
var maintenanceOperation = function (aircraft) {
    aircraft.lastMaintenanceCheck = new Date(Date.now());
    return true;
};
maintenanceOperation(customAircraft);
console.log(customAircraft.lastMaintenanceCheck);
