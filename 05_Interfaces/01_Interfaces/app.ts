// interaces define contracts 
// interfaces in javascript not only describe contracts for classes, but also for objects and functiosn 

// this interfaces defines an object with certain properties 
interface Aircraft {
    identification: string; 
    manufacturer: string;

    readonly manufacturingDate: Date; // can only be assigned during the first creation of the object
    passengerCapacity?: number; // optional parameter, does not need have a value 

    lastMaintenanceCheck: Date;
}

// the interface can be treated as a type
// this fucntion require an aircraft to be passed 
function checkAircraft(aircraft: Aircraft){
    return aircraft.identification.substring(0, aircraft.manufacturer.length) == aircraft.manufacturer; 
}

// the object impelemnts the interfaces, if it contains all the properties above
// so extra properties still makes the object an implementation of the interface
let customAircraft = {
    identification: "Airbus A380",
    manufacturer: "Airbus",
    engines: "CFM 56-5",
    manufacturingDate: new Date(Date.now()),
    lastMaintenanceCheck: new Date("2023-09-03")
};

console.log(checkAircraft(customAircraft));
console.log(customAircraft.manufacturingDate);

// this interface defines the signature of a function 
interface MaintenanceOperation{
    (aircraft: Aircraft) : boolean;
}

let maintenanceOperation: MaintenanceOperation = function(aircraft: Aircraft){
    aircraft.lastMaintenanceCheck = new Date(Date.now());
    return true; 
}

maintenanceOperation(customAircraft); 
console.log(customAircraft.lastMaintenanceCheck);

// interfaces can also be used to define the contracts for classes 
interface IExportable{
    export() : void;
}

// here the key word implements has to be used instead of extends to differentiate between interfaces and classes 
class CsvFile implements IExportable {
    
    public export(){
        console.log("Exporting csv file.");
    }

}
