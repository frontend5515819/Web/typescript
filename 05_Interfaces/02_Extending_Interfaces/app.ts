// interfaces can also be extended 
// interfaces can not be changed during runtime 
interface Mailable{
    send(email: string) : boolean;
    queue(email: string) : boolean;   
}

// however the interface can be extended 
// this interface will "inherit" the other function definitions
interface FutureMailable extends Mailable{
    later(email: string, after: number) : void;
}

// any class that implements this interface now has to implement all the methods 
class Mail implements FutureMailable{
    
    send(email: string): boolean {
        throw new Error("Method not implemented.");
    }
    queue(email: string): boolean {
        throw new Error("Method not implemented.");
    }
    later(email: string, after: number): void {
        throw new Error("Method not implemented.");
    }

}
