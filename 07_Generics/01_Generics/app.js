// generics allow to have multiple types as an input without using any 
// the function is a template for a different type 
// but depending on the argument one type is chosen and the result can use the same type again
// this avoids using type casting at the end 
function getRandomElement(elements) {
    var randomIndex = Math.floor(Math.random() * elements.length);
    return elements[randomIndex];
}
var stringArray = ["Devin", "Max", "Tony"];
var numberArray = [2, 3, 4, 1];
console.log(getRandomElement(stringArray)); // type inference makes the result a string 
console.log(getRandomElement(numberArray));
