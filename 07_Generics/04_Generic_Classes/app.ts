// generics for classes 
class Stack<T>{

    private elements : T[]; 

    constructor(){
        this.elements = [];
    }

    public push(value: T){
        this.elements.push(value); 
    }

    public pop(value: T) : T | undefined{
        return this.elements.pop(); 
    }

}

// The stack class can then be used for numbers or strings 
let stringStack = new Stack<string>(); 
let numberStack = new Stack<number>(); 