// generics can be used in interfaces to define types in methods and properties 
interface ICollection<T>{

    add(value: T) : void; 
    remove(value: T) : void; 

    getAll() : T[];

}

class List<T> implements ICollection<T>{

    private collection: T[];

    constructor(){
        this.collection = [];
    }

    public add(value: T){
        this.collection.push(value); 
    }

    public remove(value: T){
        let valueIndex = this.collection.indexOf(value); 

        if (valueIndex > -1){
            this.collection.splice(valueIndex, 1);
        }
    }

    public getAll(){
        return this.collection;
    }

}

// index types can be defined 
// here the name of the property can be dynamic
// that's it is in square brackets 
interface Options<T>{
    [name: string]: T
}

let options : Options<boolean> = {
    "visible": false, 
    "hidden": true,
};

