// to describe that only certain types in generics are allowed use the extends keyword 
// the types can only be of object type 
function safeMerge<U extends object, V extends object>(obj1: U, obj2: V){
    return {
        ...obj1,
        ...obj2,
    };
}

// this would not be allowed, since 23 is a primitive type 
// safeMerge({name: "Hoang"}, 23); 

let newObject = safeMerge({name: "Hoang"}, {age: 26});

// it also possible to specify that a type should a key of another type 
function getKey<U extends object, V extends keyof U>(obj1: U, key: V){
    return obj1[key]; 
}

// this would throw a compile time error, since age is a not property of the object 
// getKey({name: "hoang"}, "age"); 

getKey({name: "Hoang"}, "name"); 