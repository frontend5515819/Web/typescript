// the general structure of defining the types of a function 
// function (arg1: type, arg2: type) : return type

// this functions expectes two numbers and returns a number 
function add (a: number, b: number) : number{
    return a + b; 
}

// this function has no return value and is therefore of type void 
function sayHi(name: string) : void {
    console.log(`Hi ${name}`); 
}

// if the return vaklue by dynamic, then an union or any type is inferred 

