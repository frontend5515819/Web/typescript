// the rest parameters allows a function to take an infinte number of parameters 
// each of those values will be mapped to an array that can be accessed within a function 
// the rest parameter can only be at the end of a function 
function getTotal(initialValue: number, ...parameters: number[]){
    return parameters.reduce((previous, next) => previous + next, initialValue);
}

console.log(getTotal(100, 20, 30)); // 150
console.log(getTotal(100, 20, 30, 20)); // 170