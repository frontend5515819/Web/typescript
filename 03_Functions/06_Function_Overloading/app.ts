// function overloading means having functions  with the same name but different types 
// depedning on which value is inputted, a different function with the same name is used 

// function overloading works by declaruing the possible type combination first 
function add(a: number, b: number) : number;
function add(a: string, b: string) : string; 

// the last function defines the actual implementation 
// here the type is any and the function has to handle the diferent casses 
function add(a: any, b: any) : any{
    return a + b; 
}

console.log(add(1, 2)); // uses the first function 
console.log(add("Hello ", "Hoang")); // uses the second function

// function overloading will also work with methods -> functions of classes 