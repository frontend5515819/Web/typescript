// function overloading means having functions  with the same name but different types 
// depedning on which value is inputted, a different function with the same name is used 
// the last function defines the actual implementation 
// here the type is any and the function has to handle the diferent casses 
function add(a, b) {
    return a + b;
}
console.log(add(1, 2)); // uses the first function 
console.log(add("Hello ", "Hoang")); // uses the second function
