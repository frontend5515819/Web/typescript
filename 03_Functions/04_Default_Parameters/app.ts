// default parameters allow to assign defualt values ofr parameters
function applyDiscount(price: number, discount : number = 0.05){
    return price * (1 - discount); 
}

console.log(applyDiscount(100)); // 95
console.log(applyDiscount(100, 0.1)); // 90

// default paraemters are also optional parmeters 
// therefore the type of function above is (price: number, discount?: number) => number 

// it is possible to just assign the default value without specifying the type by writing =
function greet(name: string, phrase="Hello"){
    console.log(`${phrase} ${name}`);
}