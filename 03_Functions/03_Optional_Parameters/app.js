// parameters can be defined as optional 
// there passing is not required 
// in this case the variable would be undefined 
function multiply(a, b, c) {
    if (typeof c === "undefined") {
        return a * b;
    }
    return a * b * c;
}
console.log(multiply(2, 3));
console.log(multiply(2, 3, 4));
