// it is possible to define the function type before hand when declaring a function expression
// this add variable will hold a function later on that takes two number inputs and returns a number 
let addDelegate : (a: number, b: number) => number;

addDelegate = function(a: number, b: number){
    return a + b; 
}

// this function type will return nothing 
let sayHiDelegate : (name: string) => void; 

sayHiDelegate = function(name: string){
    console.log(`Hi ${name}`);
}
