// access modifiers allow tos pecifiy from where the proeprties and methods are accessible
// there are three access modifiers: 
// - public: accessible from everywhere (default if not specificed)
// - protected: only accessible from the class and any inherited class
// - private: only acecssible from within the class 
var Person = /** @class */ (function () {
    function Person(firstName, lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }
    Object.defineProperty(Person.prototype, "FullName", {
        // public is accessible everywhere 
        get: function () {
            return "".concat(this.firstName, " ").concat(this.lastName);
        },
        set: function (value) {
            var _a;
            _a = value.split(" "), this.firstName = _a[0], this.lastName = _a[1];
        },
        enumerable: false,
        configurable: true
    });
    return Person;
}());
var person = new Person("John", "Doe");
console.log(person.FullName);
person.FullName = "Hoang Nguyen Duc";
console.log(person.FullName);
