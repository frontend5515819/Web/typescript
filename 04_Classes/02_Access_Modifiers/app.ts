// access modifiers allow tos pecifiy from where the proeprties and methods are accessible
// there are three access modifiers: 
// - public: accessible from everywhere (default if not specificed)
// - protected: only accessible from the class and any inherited class
// - private: only acecssible from within the class 

class Person{

    // not accessible from the outside
    private firstName: string; 
    private lastName: string; 

    // public is accessible everywhere 
    public get FullName(){
        return `${this.firstName} ${this.lastName}`;
    }

    public set FullName(value: string){
        [this.firstName, this.lastName] = value.split(" ");
    }

    constructor(firstName: string, lastName: string){
        this.firstName = firstName; 
        this.lastName = lastName; 
    }
}

let person = new Person("John", "Doe"); 

console.log(person.FullName);

person.FullName = "Hoang Nguyen Duc";

console.log(person.FullName); 
