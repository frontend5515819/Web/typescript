var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
// inheritance allows the child class to take o er all properties and methods of the parent class 
var Employee = /** @class */ (function () {
    function Employee(fullName, role) {
        this.FullName = fullName;
        this.role = role;
    }
    Object.defineProperty(Employee.prototype, "FullName", {
        get: function () {
            return "".concat(this.firstName, " ").concat(this.lastName);
        },
        set: function (value) {
            var _a;
            _a = value.split(" "), this.firstName = _a[0], this.lastName = _a[1];
        },
        enumerable: false,
        configurable: true
    });
    Employee.prototype.work = function () {
        console.log("Working");
    };
    return Employee;
}());
var Developer = /** @class */ (function (_super) {
    __extends(Developer, _super);
    function Developer(fullName) {
        return _super.call(this, fullName, "Developer") || this;
    }
    // overwrites the parents class 
    Developer.prototype.work = function () {
        console.log("Programming.");
    };
    return Developer;
}(Employee));
var developer = new Developer("Hoang Nguyen");
console.log(developer.FullName);
developer.work();
