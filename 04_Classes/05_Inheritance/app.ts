// inheritance allows the child class to take o er all properties and methods of the parent class 
class Employee {

    protected firstName: string; 
    protected lastName: string; 

    protected role: string; 

    public get FullName() {
        return `${this.firstName} ${this.lastName}`; 
    }

    public set FullName(value: string) {
        [this.firstName, this.lastName] = value.split(" "); 
    }

    constructor(fullName: string, role: string){
        this.FullName = fullName; 
        this.role = role; 
    }

    public work() : void{
        console.log("Working"); 
    }

}

class Developer extends Employee{

    constructor(fullName: string){
        super(fullName, "Developer"); 
    }

    // overwrites the parents class 
    public work() : void {
        console.log("Programming.");
    }

}

let developer = new Developer("Hoang Nguyen");
console.log(developer.FullName);
developer.work();