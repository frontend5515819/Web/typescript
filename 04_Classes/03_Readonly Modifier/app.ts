// the readonly property can only be assigned once on declaration on in the constructor 
// afterwards a assignment is not allowed 
class Building {

    public readonly Address: string; 

    constructor(address: string){
        this.Address = address;
    }

}

let building = new Building("Neugutstrasse 4"); 

// the address can not be assigned agian 
// building.Address = "Zimmerweg 3";