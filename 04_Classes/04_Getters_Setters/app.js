// getters and setters are methods to access certain data from the class
// getters and setters can define logic how to get certain data or how to assign the data 
var Airport = /** @class */ (function () {
    function Airport(icao) {
        if (icao) {
            this.Icao = icao;
        }
    }
    Object.defineProperty(Airport.prototype, "Icao", {
        get: function () {
            return this._icao;
        },
        set: function (value) {
            if (value.length != 4) {
                throw new Error("ICAO should have a length of 4");
            }
            this._icao = value;
        },
        enumerable: false,
        configurable: true
    });
    return Airport;
}());
var londonHeathrow = new Airport("EGLL");
try {
    var customAirport = new Airport();
    customAirport.Icao = "AS";
}
catch (e) {
    console.log(e.message);
}
