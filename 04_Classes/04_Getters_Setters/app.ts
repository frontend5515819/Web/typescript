// getters and setters are methods to access certain data from the class
// getters and setters can define logic how to get certain data or how to assign the data 
class Airport {

    private _icao : string; 

    public get Icao(){
        return this._icao; 
    }

    public set Icao(value: string){
        if (value.length != 4){
            throw new Error("ICAO should have a length of 4"); 
        }

        this._icao = value; 
    }

    constructor(icao?: string){
        if (icao){
            this.Icao = icao; 
        }
    }
}

let londonHeathrow = new Airport("EGLL");

try{
    let customAirport = new Airport(); 
    customAirport.Icao = "AS";
}catch(e){
    console.log(e.message);
}
