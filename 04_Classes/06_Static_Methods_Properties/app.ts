class Registration {

    // static variables are tied to the class and not to the obejct
    private static count: number; 

    constructor(){
        Registration.count++;
    }

    // within a static method, only static variables can be accessed
    public static get Count(){
        return Registration.count;
    }

}

let registration1 = new Registration();
let registration2 = new Registration();

// accessible from the class level
console.log(Registration.Count); 