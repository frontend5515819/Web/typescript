// classes can be deinfed already in javscript 
// typescript allows to add types to each proeprty and methods
class Car{

    name: string; // defintion of properties
    brand: string; 

    _price: number; 

    constructor(name: string, brand: string){
        this.name = name; 
        this.brand = brand; 
    }

    getCarType() : string { // method can have return types as well
        return `${this.brand} ${this.name}`;
    }

    get Price(){
        return this._price; 
    }

    set Price(value: number){
        this._price = value;
    }
}

let car = new Car("A4", "Audi"); 

console.log(car.getCarType());

car.Price =  24000;

console.log(car.Price);