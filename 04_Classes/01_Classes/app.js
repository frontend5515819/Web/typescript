// classes can be deinfed already in javscript 
// typescript allows to add types to each proeprty and methods
var Car = /** @class */ (function () {
    function Car(name, brand) {
        this.name = name;
        this.brand = brand;
    }
    Car.prototype.getCarType = function () {
        return "".concat(this.brand, " ").concat(this.name);
    };
    Object.defineProperty(Car.prototype, "Price", {
        get: function () {
            return this._price;
        },
        set: function (value) {
            this._price = value;
        },
        enumerable: false,
        configurable: true
    });
    return Car;
}());
var car = new Car("A4", "Audi");
console.log(car.getCarType());
car.Price = 24000;
console.log(car.Price);
