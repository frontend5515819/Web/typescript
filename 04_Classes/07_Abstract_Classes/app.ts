// abstract classes can not be instantiated directly 
abstract class Furniture{

    // as an abstract method, any class that extends this class, this method has to be impelmented
    abstract getPrice(): number; 

}

class Chair extends Furniture{
    public getPrice() : number{
        return 12; 
    }
}

// this would throw a compile time error 
// let chair = new Furniture();