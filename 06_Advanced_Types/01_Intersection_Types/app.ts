// intersection types allow to combine different types together 
// in this case the type has to define the properties and methods of both types 
interface Identity {
    id: number; 
    name: string; 
}

interface Contact {
    email: string; 
    phone: string; 
}

// the Student type has to now implement both types 
// the order of the types do not matter 
type Student = Contact & Identity; 

let student : Student = {
    id: 12, 
    name: "Devin",
    email: "devin@gmail.com",
    phone: "+41 2321232"
}

// intersection types is a combination of types 
// union types means the type can be either one or the other

