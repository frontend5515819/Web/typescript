// Type casting is transforming a variable from one type to another 
// One way is to use the as operaetor 
var input = document.querySelector("input[type='text']") as HTMLInputElement; 

// only casting the input into a HTMLINput element, the value property can be used 
console.log(input.value); 

// the same can be done using the <> operator
var secondInput = <HTMLInputElement>document.querySelector("input[type='text']");

console.log(secondInput.value);