type alphanumeric = number | string; 

function add (a: alphanumeric, b: alphanumeric){

    // typescript is aware that the type is checked here for a and b
    // therefore inside the method and the arguments are treated as numbers 
    if (typeof a === "number" && typeof b === "number"){
        return a + b; 
    }
    else if (typeof a === "string" && typeof b === "string"){
        return a.toLowerCase() + b.toLowerCase(); 
    }

    throw new Error("Invalid arguments.");

}

class Customer {
    public isCreditAllowed(): boolean {
        return true; 
    }
}

class Supplier {
    public isInShortList(): boolean {
        return false; 
    }
}

type BusinessPartner = Customer | Supplier; 

function signContract(businessParter: BusinessPartner) {

    // typescript knows that the type of the argument is checked 
    // inside the if clause the businessParter var can be treated as an instance of Customer
    if (businessParter instanceof Customer){
        return businessParter.isCreditAllowed();  
    }

    else if (businessParter instanceof Supplier){
        return businessParter.isInShortList(); 
    }

}

function signContractv2(businessParter: BusinessPartner) {

    // typescript allows to check for a property name in the object
    if ("isCreditAllowed" in businessParter){
        return businessParter.isCreditAllowed();  
    }

    else if (businessParter instanceof Supplier){
        return businessParter.isInShortList(); 
    }

}

// user defined type guards are functions that return (var is type)
function isCustomer(partner: any) : partner is Customer {
    return partner instanceof Customer; 
}

function signContractv3(businessParter: BusinessPartner) {

    // typescript allows to check for a property name in the object
    if (isCustomer(businessParter)){
        return businessParter.isCreditAllowed();  
    }

    else if (businessParter instanceof Supplier){
        return businessParter.isInShortList(); 
    }

}