// array types are inferred automatically 
var names = ["John", "Max"];
// the array can be extended by adding new elements to it 
names[2] = "Tim";
names[3] = "Josh";
names.push("Teddy");
console.log(names);
// arrays come with some built in methods 
console.log(names.length);
console.log(names.map(function (name) { return name[0].toLocaleLowerCase() + name.substring(1).toUpperCase(); }));
