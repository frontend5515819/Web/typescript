// array types are inferred automatically 
let names = ["John", "Max"]; 

// the array can be extended by adding new elements to it 
names[2] = "Tim"; 
names[3] = "Josh"; 
names.push("Teddy");

console.log(names); 

// arrays come with some built in methods 
console.log(names.length); 
console.log(names.map(name => name[0].toLocaleLowerCase() + name.substring(1).toUpperCase()));

// if values are of different types, the array is a mixed array 
let stringAndNumbersArray : (string | number)[]; 
stringAndNumbersArray = ["Hello", 23, "my", 1];