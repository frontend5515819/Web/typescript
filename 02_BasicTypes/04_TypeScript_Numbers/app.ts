// numbers can be integers or floating numbers
let myInteger : number = 10; 
let myFloating : number = 12.22; 

// numbers can also be binary 
let myBinary = 0b00001010; 

console.log(myBinary);