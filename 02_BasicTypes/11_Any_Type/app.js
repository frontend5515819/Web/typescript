// the any type tells the compiler to allow any type for the value
// it will instruct the typescript compiler to skip type checking for that variable 
var json = '{ "latitude": 30.345, "longitude": 123.21}';
var coordinate = JSON.parse(json);
console.log(coordinate.latitude);
console.log(coordinate.longitude);
