// the any type tells the compiler to allow any type for the value
// it will instruct the typescript compiler to skip type checking for that variable 
const json = '{ "latitude": 30.345, "longitude": 123.21}'; 

let coordinate = JSON.parse(json); 

console.log(coordinate.latitude); 
console.log(coordinate.longitude); 

// the coordinate is of any type, since the typescript can not infer the type 
// so acessing not defined properties will not throw a compile time error 
console.log(coordinate.name); // undefined 

// the any type allows a way to migrate old javascript projects 
let mixedVariable : any = "Hoang"; 
mixedVariable = 12; 

