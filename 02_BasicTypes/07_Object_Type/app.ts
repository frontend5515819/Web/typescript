// The object type defines all types that are not primitive 
// object types can not be assigned primitives types 
let myUser : {
    firstName: string; 
    lastName: string; 
    age: number; 
};

// This would throw a compile time error 
// myUser = "Hello"

// Furthermore all properties of an object needs to be defined 
myUser = {
    firstName: "Hoang",
    lastName: "Nguyen",
    age: 26
};

// any assignmetn of new properties are not allowed 
// myUser.isAdmin = false; 

// declaration and assignment can be done in together like this 
let animal : {
    name: string, 
    canJump: boolean, 
} = {
    name: "Rabbit",
    canJump: true 
}; 

// There is also an empty tpe, where no new properties can be added 
let emtpyObject = {};
