// strings can be placed as types 
// string literals will force the variable to only have that string as a value 
let appEvent : "click";

// this would throw an error 
// appEvent = "test";

// it has to assign the string literal as a value 
appEvent = "click";

// Combined the union type, variables can be forced to have a predefined set of strings 
let clickEvent : "mouseDown" | "mouseUp" | "mouseWheel"; 

clickEvent = "mouseUp";