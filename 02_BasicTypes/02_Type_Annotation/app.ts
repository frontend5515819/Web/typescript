// normally the type is inferred based on the usage of the variable 
// but typescript allows to annotate types by adding the type after the : 
let myString : string = "Hello"; 
let myNumber : number = 12.3; 
let myBoolean : boolean = false; 

// this would throw a compile time error 
// myString = 3; 

// objects can also be defined before hand 
let myObject : {
    firstName : string;
    lastName : string;
    age: number;
}; 

// objects can then only defined by the properties that were given before 
myObject = {
    firstName: "Hoang",
    lastName: "Nguyen",
    age: 26
};

// functions can also define the input types and return type 
function add(a : number, b: number) : number{
    return a + b; 
}

let greeting : (name: string) => string; 

// this may work as well for function expressions 
greeting = function (name: string) : string {
    return `Hi ${name}`;
};

console.log(add(2, 3)); 