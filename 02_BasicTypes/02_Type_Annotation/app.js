// normally the type is inferred based on the usage of the variable 
// but typescript allows to annotate types by adding the type after the : 
var myString = "Hello";
var myNumber = 12.3;
var myBoolean = false;
// this would throw a compile time error 
// myString = 3; 
// objects can also be defined before hand 
var myObject;
// objects can then only defined by the properties that were given before 
myObject = {
    firstName: "Hoang",
    lastName: "Nguyen",
    age: 26
};
// functions can also define the input types and return type 
function add(a, b) {
    return a + b;
}
// this may work as well for function expressions 
var greeting = function (name) {
    return "Hi ".concat(name);
};
console.log(add(2, 3));
