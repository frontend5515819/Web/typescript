// the union type allows a variable to have values of a selection of types 
// the union type is therefore the middle between any type and a specific type 

// Imagine the add function should only take strings and numbers 
// using any would totally skip type chkcing which is sub optimal 
// therefore the union can be used 
function add (item1 : number | string, item2 : number | string) : number | string {

    if (typeof item1 === "number" && typeof item2 === "number"){
        return item1 + item2; 
    }
    else if (typeof item1 === "string" && typeof item2 === "string"){
        return item1 + item2; 
    }

    throw new Error("Input values have to be numbers or strings"); 
}

console.log(add(12, 32)); 
console.log(add("12", "32")); 

let unionVariable : number | string | boolean; 

unionVariable = 12;
unionVariable = "Hello"; 
unionVariable = false; 

