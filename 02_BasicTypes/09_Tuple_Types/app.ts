// Tuples are fixed size arrays that can have different value types 
// tuples are defined as follow: 
let skill : [string, number]; 
skill = ["Programming", 5];

// tuples can have optional values 
let rgba : [number, number, number, number?]; 

// both are valid tuples according to the type above 
rgba = [100, 200, 10, 30]; 
rgba = [100, 200, 10]; 