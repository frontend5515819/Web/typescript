// Similar to javascript, strings are supported by single and double quotes 
var singleQuoteString = 'Single Quotes';
var doubleQuotesString = "Double Quotes";
// Also string interpolation through template strings are possible 
var templateString = "My age is ".concat(26);
console.log(templateString);
