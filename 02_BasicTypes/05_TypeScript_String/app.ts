// Similar to javascript, strings are supported by single and double quotes 
let singleQuoteString = 'Single Quotes'; 
let doubleQuotesString = "Double Quotes"; 

// Also string interpolation through template strings are possible 
let templateString = `My age is ${26}`; 

console.log(templateString); 