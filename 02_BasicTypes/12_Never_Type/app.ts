// the never type allows a variable to not be assigned to anything 
// this is used for functions that throws errors 
// in this case the return type would be never, since no variable can be assigned to an error 
function raiseError(errorMessage : string) : never {
    throw new Error(errorMessage); // would never return an actual value 
}

// This would be also the case if the execution never leaves the function 
let infiniteLoop = function(){
    while(true){
        console.log("...");
    }
}