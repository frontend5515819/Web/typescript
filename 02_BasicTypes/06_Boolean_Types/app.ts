// boolean types 
let myBool : boolean = true; 

// here are some logical operators to use
let myBool2 : boolean = false; 

console.log(myBool && myBool2); // AND operator
console.log(myBool || myBool2); // OR operator
console.log(!myBool); // Negation operator