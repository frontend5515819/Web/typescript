// boolean types 
var myBool = true;
// here are some logical operators to use
var myBool2 = false;
console.log(myBool && myBool2); // AND operator
console.log(myBool || myBool2); // OR operator
console.log(!myBool); // Negation operator
