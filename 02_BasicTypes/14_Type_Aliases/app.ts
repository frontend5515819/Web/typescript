// type aliases allows to reference a type by a different name 
type chars = string; 

// now the new chars type can be used 
let myChars : chars = "Hello"; 

// this is especially helpful for unions 
type alphanumeric = string | number; 

let myAlphaNumberic : alphanumeric = "Hello"; 
myAlphaNumberic = 12; 