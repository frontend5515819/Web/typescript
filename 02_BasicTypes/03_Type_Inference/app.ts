// Types can be most of the time be inferred 
// by assignment: 
let number1 = 100;

// this would be equivalent to 
let numebr2 : number = 100; 

// Array types are inferred by their content 
// This would be a string array
let stringArray = ["Hello", "my"];

// This would be a mixes array (string | number)[]
let mixedArray = ["Hello", 12];


