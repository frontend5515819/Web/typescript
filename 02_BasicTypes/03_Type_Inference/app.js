// Types can be most of the time be inferred 
// by assignment: 
var number1 = 100;
// this would be equivalent to 
var numebr2 = 100;
// Array types are inferred by their content 
var stringArray = ["Hello", "my"];
console.log(typeof stringArray);
