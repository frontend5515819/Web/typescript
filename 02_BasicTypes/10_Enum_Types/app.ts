// Enumerations are list of constant values 
enum Roles {
    Admin, 
    Editor = 2, // the number can be specified 
    ReadOnly
};

// enums are treated as a type and can specify the type of a property 
let user : {
    loginName: string; 
    role: Roles; 
}

// assigning the value for the roles
user = {
    loginName: "johndoe97",
    role: Roles.Admin
}

// each value is in fact just a number ior integer 
// the enum itself is an object with the enum names as property names assiging interger values 
console.log(Roles.Admin); // 0
console.log(Roles.Editor); // 2
console.log(Roles.ReadOnly); // 3

