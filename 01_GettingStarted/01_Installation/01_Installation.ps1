# Typescript is a superset of Javascript 
# This means that all Javascript code is also valid Typescript Code 
# However Typescript also offers typing and other functionalities 
# Typescript will then convert the code into Javascript by the Typescript compiler 
# During the compilation, wrong data types can be resolved before they happen during runtime 

# Installing node.js is required 
# Isntalling the typescript compiler through npm 
npm install -g typescript

# Showing the version of typescript 
tsc -v 

# Downloading the typescript package for node.js 
npm install -g ts-node