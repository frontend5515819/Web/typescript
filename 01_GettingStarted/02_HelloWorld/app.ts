// type annotation for a string 
let message : string = "Hello World"; 

console.log(message); 

// the app.ts file is compiled by: tsc app.ts
// the app.js file can then be executed 